x=$1
echo generating $x
mkdir -p out
convert voucher.png -font courier-bold -gravity center -fill '#585d80' -pointsize 32 \
  -draw "text 0,100 '$x'" out/voucher_$x.png
