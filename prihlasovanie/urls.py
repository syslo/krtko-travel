from django.urls import path
from . import views

urlpatterns = [
    path('redeem/', views.redeem, name='redeem'),
    path('counter/', views.counter, name='counter'),
    path('<code>/', views.profile, name='profile'),
    path('<code>/prihlaska/', views.registration, name='registration'),
]
