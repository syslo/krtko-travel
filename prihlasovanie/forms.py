from django.forms import ModelForm, DateInput, BooleanField

from .models import Registration

class RegistrationForm(ModelForm):
    tocs = BooleanField(
        required=True,
        label="V súlade s článkom 13 všeobecného nariadenia na reguláciu dát súhlasím so spracovaním mojich osobných údajov v rozsahu potrebnom na účely ubytovania a internej evidencie občianskeho združenia Trojsten s prípadnou možnosťou zahrnutia v budúcej propagácii akcií organizovaných občianskym združením Trojsten. Súhlas poskytujem na dobu nevyhnutnú v minimálnom rozsahu "
    )

    class Meta:
        model = Registration
        exclude = ("voucher",)
        widgets = {'birthdate': DateInput(attrs={'type':'date'})}
