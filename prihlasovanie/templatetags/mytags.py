from django import template
from django.utils.safestring import mark_safe

register = template.Library()

@register.simple_tag
def li_todo(condition):
    if condition:
        return mark_safe("""<li class="list-group-item list-group-item-success">&#x2714;&nbsp;&nbsp;""")
    else:
        return mark_safe("""<li class="list-group-item list-group-item-danger">&#x2718;&nbsp;&nbsp;""")
