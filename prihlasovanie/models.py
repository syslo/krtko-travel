from django.db import models
import random

# Create your models here.

def voucher_code_generator_16():
    return "".join(random.choices("0123456789abcdefghijklmnopqrstuvwxyz", k=16))

class Voucher(models.Model):
    code = models.CharField(
        max_length=16,
        default=voucher_code_generator_16,
        verbose_name="Kód",
        primary_key=True,
    )

    reserved_for = models.TextField(
        blank=True,
        verbose_name="Rezervované pre",
    )

    from_prince = models.BooleanField(
        blank=False,
        verbose_name="Pozvánka od princa",
    )

    is_demo = models.BooleanField(
        blank=False,
        default=False,
        verbose_name="Toto je testovací kód",
    )

    def __str__(self):
        return "%sVoucher %s" % ("[TEST] " if self.is_demo else "", self.code)


class Registration(models.Model):
    voucher = models.OneToOneField(
        Voucher,
        on_delete=models.CASCADE,
        related_name='registration',
        blank=False,
        editable=False,
    )
    name = models.CharField(
        blank=False,
        max_length=400,
        verbose_name="Meno a priezvisko",
    )
    birthdate = models.DateField(
        blank=False,
        verbose_name="Dátum narodenia",
    )
    street = models.CharField(
        blank=False,
        max_length=400,
        verbose_name="Ulica a číslo",
    )
    postcode = models.CharField(
        blank=False,
        max_length=10,
        verbose_name="PSČ",
    )
    city = models.CharField(
        blank=False,
        max_length=80,
        verbose_name="Mesto",
    )
    phone = models.CharField(
        blank=False,
        max_length=20,
        verbose_name="Tvoje telefónne číslo",
    )
    email = models.EmailField(
        blank=False,
        verbose_name="Tvoj e-mail",
    )
    phone_parent = models.CharField(
        blank=False,
        max_length=20,
        verbose_name="Telefónne číslo rodiča",
    )
    email_parent = models.EmailField(
        blank=False,
        verbose_name="E-mail rodiča",
    )
    alergies = models.TextField(
        blank=True,
        verbose_name="Zdravotné špecifikácie (alergie, lieky, diéty a iné)",
    )
    notes = models.TextField(
        blank=True,
        verbose_name="Chceš nám ešte niečo povedať?",
    )

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["voucher", "name"]
        verbose_name = "Prihláška"
        verbose_name_plural = "Prihlášky"
