from django.shortcuts import get_object_or_404, render
from django.http import Http404, HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages
from django.db import IntegrityError

from .models import Voucher
from .forms import RegistrationForm


def redeem(request):
    code = request.GET.get('voucher','')
    try:
        voucher = Voucher.objects.get(code=code)
    except Voucher.DoesNotExist:
        return HttpResponseRedirect("/?invalid=true&voucher=%s" % code)

    return HttpResponseRedirect(reverse('profile', args=(code,)))


def profile(request, code):

    try:
        voucher = Voucher.objects.get(code=code)
    except Voucher.DoesNotExist:
        raise Http404("Voucher does not exist")

    if not hasattr(voucher, 'registration'):
        messages.info(request, "Prosím, vyplňte prihlášku")
        return HttpResponseRedirect(reverse('registration', args=(code,)))

    return render(request, 'prihlasovanie/profile.html', {
        'voucher': voucher
    })


def registration(request, code):
    voucher = get_object_or_404(Voucher, code=code)

    if hasattr(voucher, 'registration'):
        messages.info(request, "Pre tento voucher už bola prihláška vyplnená!")
        return HttpResponseRedirect(reverse('profile', args=(code,)))

    if request.method == 'POST':
        form = RegistrationForm(request.POST)

        if form.is_valid():
            try:
                registration = form.save(commit=False)
                registration.voucher = voucher
                registration.save()
                messages.success(request, "Prihláška bola vyplnená")
                return HttpResponseRedirect(reverse('profile', args=(code,)))
            except IntegrityError:
                messages.error(request, "Pre tento voucher už bola prihláška vyplnená!")
    else:
        form = RegistrationForm()

    return render(request, 'prihlasovanie/registration.html', {
        'voucher': voucher,
        'form': form,
    })


def counter(request):
    production_vouchers = Voucher.objects.exclude(is_demo=True)
    registered_vouchers = production_vouchers.exclude(registration__isnull=True)
    unregistered_vouchers = production_vouchers.filter(registration__isnull=True)
    normal_count = registered_vouchers.filter(from_prince=False).count()
    adventure_count = registered_vouchers.filter(from_prince=True).count()
    reserved_count = unregistered_vouchers.exclude(reserved_for__isnull=True).exclude(reserved_for__exact='').count()
    return render(request, 'prihlasovanie/counter.html', {
        'normal_count': normal_count,
        'adventure_count': adventure_count,
        'reserved_count': reserved_count,
        'free_count': 42 - normal_count - adventure_count - reserved_count,
    })
