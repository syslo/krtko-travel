from django.contrib import admin

from .models import Voucher, Registration

class RegistrationInline(admin.StackedInline):
    model = Registration
    classes = ['collapse']
    extra = 0

    def has_delete_permission(self, request, obj=None):
        return request.user.username == "sysel"


@admin.register(Voucher)
class VoucherAdmin(admin.ModelAdmin):
    list_display=('__str__', 'is_demo', 'from_prince', 'reserved_for', 'registration',)
    readonly_fields=()
    inlines = (
        RegistrationInline,
    )

    def get_readonly_fields(self, request, obj=None):
        if obj: # editing an existing object
            return self.readonly_fields + ('code',)
        return self.readonly_fields

    def has_delete_permission(self, request, obj=None):
        return request.user.username == "sysel"
