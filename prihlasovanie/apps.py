from django.apps import AppConfig


class PrihlasovanieConfig(AppConfig):
    name = 'prihlasovanie'
